package com.infosys.learnmanagementservice.service;

import java.util.List;

import com.infosys.learnmanagementservice.model.Publisher;

public interface PublisherService {

	public List<Publisher> findAll();
	
	public Publisher findOne(int id);
	
	public Publisher save(Publisher publisher);
	
	public void delete(int id);
}

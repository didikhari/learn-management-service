package com.infosys.learnmanagementservice.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.infosys.learnmanagementservice.model.Publisher;
import com.infosys.learnmanagementservice.repository.PublisherRepository;

@Service
public class PublisherServiceImpl implements PublisherService {

	@Autowired
	private PublisherRepository publisherRepository;
	
	public List<Publisher> findAll() {
		return publisherRepository.findAll();
	}

	@Override
	public Publisher findOne(int id) {
		return publisherRepository.findById(id).orElse(null);
	}

	@Override
	public Publisher save(Publisher publisher) {
		return publisherRepository.save(publisher);
	}

	@Override
	public void delete(int id) {
		publisherRepository.deleteById(id);
	}
	
	
}

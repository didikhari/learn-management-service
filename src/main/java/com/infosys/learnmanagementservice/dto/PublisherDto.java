package com.infosys.learnmanagementservice.dto;

import lombok.Data;

@Data
public class PublisherDto {
	private int id;
	private String name;
}

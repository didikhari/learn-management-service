package com.infosys.learnmanagementservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.infosys.learnmanagementservice.model.Publisher;

public interface PublisherRepository extends JpaRepository<Publisher, Integer> {

}

package com.infosys.learnmanagementservice.web;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.infosys.learnmanagementservice.dto.PublisherDto;
import com.infosys.learnmanagementservice.dto.ResponseDto;
import com.infosys.learnmanagementservice.model.Publisher;
import com.infosys.learnmanagementservice.service.PublisherService;

@RestController
@RequestMapping(value = "/v1/publishers")
public class PublisherController {

	@Autowired
	private PublisherService publisherService;
	
	@Autowired
	private ModelMapper modelMapper;
	
	@GetMapping(value = {"", "/"})
	public List<PublisherDto> getAll() {
		List<Publisher> results = publisherService.findAll();
		return results.stream()
				.map(v -> modelMapper.map(v, PublisherDto.class))
				.collect(Collectors.toList());
	}
	
	@GetMapping(value = "/{id}")
	public PublisherDto get(@PathVariable int id) {
		Publisher result = publisherService.findOne(id);
		return modelMapper.map(result, PublisherDto.class);
	}
	
	@PostMapping(value = {"", "/"})
	public PublisherDto add(@RequestBody PublisherDto requestData) {
		Publisher publisher = modelMapper.map(requestData, Publisher.class);
		Publisher result = publisherService.save(publisher);
		return modelMapper.map(result, PublisherDto.class);
	}
	
	@PutMapping(value = "/{id}")
	public PublisherDto update(@PathVariable int id, @RequestBody PublisherDto requestData) {
		Publisher publisher = modelMapper.map(requestData, Publisher.class);
		publisher.setId(id);
		Publisher result = publisherService.save(publisher);
		return modelMapper.map(result, PublisherDto.class);
	}
	
	@DeleteMapping(value = "/{id}")
	public ResponseDto delete(@PathVariable int id) {
		publisherService.delete(id);
		return ResponseDto.builder()
				.status("success")
				.description("Publisher deleted")
				.build();
	}
}
